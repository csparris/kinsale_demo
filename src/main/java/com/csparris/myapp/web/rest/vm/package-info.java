/**
 * View Models used by Spring MVC REST controllers.
 */
package com.csparris.myapp.web.rest.vm;
